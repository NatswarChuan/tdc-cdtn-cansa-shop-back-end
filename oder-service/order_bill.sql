-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3308
-- Thời gian đã tạo: Th12 11, 2021 lúc 05:24 AM
-- Phiên bản máy phục vụ: 10.6.4-MariaDB-log
-- Phiên bản PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `order_bill`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order`
--

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `oder_id` varchar(249) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oder_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oder_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oder_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `oder_customer` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`oder_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11632497116230 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `order`
--

INSERT INTO `order` (`oder_id`, `oder_address`, `oder_phone`, `oder_date`, `oder_customer`, `status`) VALUES
('11632500884297', '34k,Phường Phước Nguyên,Thành phố Bà Rịa,Tỉnh Bà Rịa - Vũng Tàu', '0908713697', '2021-09-24 16:28:04', 1, 4),
('11632500884201', '34k,Phường Phước Nguyên,Thành phố Bà Rịa,Tỉnh Bà Rịa - Vũng Tàu', '0908713697', '2021-09-24 16:28:04', 1, 1),
('11632500884299', '34k,Phường Phước Nguyên,Thành phố Bà Rịa,Tỉnh Bà Rịa - Vũng Tàu', '0908713697', '2021-09-24 16:28:04', 1, 1),
('11632500884200', '34k,Phường Phước Nguyên,Thành phố Bà Rịa,Tỉnh Bà Rịa - Vũng Tàu', '0908713697', '2021-09-24 16:28:04', 1, 1),
('11632500884298', '34k,Phường Phước Nguyên,Thành phố Bà Rịa,Tỉnh Bà Rịa - Vũng Tàu', '0908713697', '2021-09-24 16:28:04', 1, 1),
('11636966657075', '34k,Phường Phước Nguyên,Thành phố Bà Rịa,Tỉnh Bà Rịa - Vũng Tàu', '0908713697', '2021-11-15 08:57:37', 1, 3),
('11638975599607', 'Ggggg,Phường 14,Quận Bình Thạnh,Thành phố Hồ Chí Minh', 'Ggggg,Phường 14,Quận Bình Thạnh,Thành phố Hồ Chí Minh', '2021-12-08 14:59:59', 1, 1),
('1241639037674560', 'Pham Anh\n,Xã Hòa Bình Thạnh,Huyện Châu Thành,Tỉnh An Giang', 'Pham Anh\n,Xã Hòa Bình Thạnh,Huyện Châu Thành,Tỉnh An Giang', '2021-12-09 08:14:34', 124, 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_oder`
--

DROP TABLE IF EXISTS `product_oder`;
CREATE TABLE IF NOT EXISTS `product_oder` (
  `oder_id` varchar(249) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `product_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` double NOT NULL,
  `product_avatar` int(11) NOT NULL,
  `product_sale` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`,`oder_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_oder`
--

INSERT INTO `product_oder` (`oder_id`, `product_id`, `shop_id`, `product_quantity`, `status`, `product_title`, `product_price`, `product_avatar`, `product_sale`) VALUES
('11632500884299', 1, 1, 2, 1, 'Quần lót nữ đúc su thun lạnh cạp ép không viền cao cấp QL16', 10900, 1, 0),
('11632500884299', 21, 2, 4, 1, 'Quần lót nữ đúc su thun lạnh cạp ép không viền cao cấp QL16', 10900, 61, 0),
('11632500884298', 21, 2, 4, 1, 'Quần lót nữ đúc su thun lạnh cạp ép không viền cao cấp QL16', 10900, 61, 0),
('11632500884298', 1, 1, 2, 1, 'Quần lót nữ đúc su thun lạnh cạp ép không viền cao cấp QL16', 10900, 1, 0),
('11632500884297', 1, 1, 2, 4, 'Quần lót nữ đúc su thun lạnh cạp ép không viền cao cấp QL16', 10900, 1, 0),
('11632500884297', 21, 2, 4, 4, 'Quần lót nữ đúc su thun lạnh cạp ép không viền cao cấp QL16', 10900, 61, 0),
('11632500884201', 1, 1, 2, 1, 'Quần lót nữ đúc su thun lạnh cạp ép không viền cao cấp QL16', 10900, 1, 0),
('11632500884201', 21, 2, 4, 1, 'Quần lót nữ đúc su thun lạnh cạp ép không viền cao cấp QL16', 10900, 61, 0),
('11632500884200', 1, 1, 2, 1, 'Quần lót nữ đúc su thun lạnh cạp ép không viền cao cấp QL16', 10900, 1, 0),
('11632500884200', 21, 2, 4, 1, 'Quần lót nữ đúc su thun lạnh cạp ép không viền cao cấp QL16', 10900, 61, 0),
('11636966493359', 3, 1, 3, 1, 'Quần lót nữ thun lạnh đúc su không đường may mặc trong váy dễ thương QL01', 10900, 7, 0),
('11636966657075', 3, 1, 3, 3, 'Quần lót nữ thun lạnh đúc su không đường may mặc trong váy dễ thương QL01', 10900, 7, 0),
('11636966657075', 7, 1, 1, 3, 'Quần ống suông rộng dáng dài loại đẹp Đen - Tím', 99000, 19, 0),
('11638975599607', 38, 2, 1, 1, '?HOT TREND? Đầm ôm body da beo Cao Cấp Màu Sắc Nhã Nhặn Thật Sự Phù Hợp Để Tới Công Sở, Đi Làm Hoặc Đi Chơi', 139000, 112, 0),
('11638975599607', 1, 1, 1, 1, 'Quần lót nữ đúc su thun lạnh cạp ép không viền cao cấp QL16', 10900, 1, 0),
('11638975599607', 30, 2, 1, 0, '?HOT TREND? Đầm Ôm Body REN Cao Cấp Màu Sắc Nhã Nhặn Thật Sự Phù Hợp Để Tới Công Sở, Đi Làm Hoặc Đi Chơi,Free Size<57', 13000, 88, 0),
('1241639037674560', 56, 2, 1, 1, '(50kg-95kg)Thun Lạnh, Co Giãn 4 Chiều, Logo Phản Quang, Áo Thun Nam,', 55000, 166, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
